/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package lab4.ebj;

import jakarta.ejb.Stateless;
import java.util.Date;
//import javax.ejb.Stateless;

/**
 *
 * @author wojdak
 */
@Stateless
public class DataAccessBean implements DataAccessBeanRemote {
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    private String OstatnieDane;
    private Date OstatniDostep;
    
    @Override
    public String getOstatnieDane()  {
        return OstatniDostep + "\n" + OstatnieDane;
    }

    @Override
    public void setOstatnieDane(String dane, Date data)  {
        OstatnieDane = dane;
        OstatniDostep = data;
    }
}
